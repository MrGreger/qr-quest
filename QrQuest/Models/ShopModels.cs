﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QrQuest.Models
{
    public enum OrderState
    {
        Ok,
        WaitingForPurchase,
        Canseled
    }

    public class Subscription
    {
        public int Id { get; set; }
   
        public int Cost { get; set; }
        public TimeSpan Time { get; set; }
    }

    public class SubscriptionOrder
    {
        public int Id { get; set; }
        public Subscription Subscription { get; set; }
        public OrderState OrderState { get; set; }
        public User Customer { get; set; }
        public string InvId { get; set; }
    }

    public class Order
    {
        public int Id { get; set; }
        public Game Product { get; set; }
        public User Customer { get; set; }
        public OrderState OrderState { get; set; }
        public decimal Sum { get; set; }
        public string InvId { get; set; }
    }
}
