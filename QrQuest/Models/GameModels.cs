﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using LiteDB;
using Microsoft.AspNetCore.Identity;
using Newtonsoft.Json;
using QrQuest.Models.ViewModels;

namespace QrQuest.Models
{
    public class Game
    {
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        public string Description  { get; set; }
        public List<Quest> Quests { get; set; }
        public User Creator { get; set; }
        public string FinishMessage { get; set; }
        public int? AdditionalRating { get; set; }
        public decimal? Cost { get; set; }
        public bool AccessByCode { get; set; }
        public string AccessCode { get; set; }
    }

    public class Quest
    {
        public int Id { get; set; }
        public string Question { get; set; }
        //public List<string> Materials { get; set; }
        public string Answer { get; set; }
        public string Address { get; set; }

        public Location Location { get; set; }

        [JsonIgnore]
        [BsonIgnore]
        public Game Game { get; set; }
        [JsonIgnore]
        [BsonIgnore]
        public int? GameId { get; set; }
    }

    public class User : IdentityUser
    {
        private bool _creator;
        public int Rating { get; set; }

        public string Password { get; set; }

        [NotMapped]
        public bool Creator
        {
            get { return DateTime.Now < SubscriptionEnd; }
            set { _creator = value; }
        }

        public DateTime SubscriptionEnd { get; set; }
    }

    public class GameSession
    {
        public int Id { get; set; }
        public User User { get; set; }
        public Game Game { get; set; }
        public int CurrentQuest { get; set; }
    }


}
