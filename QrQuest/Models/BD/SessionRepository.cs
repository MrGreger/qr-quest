﻿using LiteDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QrQuest.Models
{
    public class SessionRepository : IRepository<GameSession>
    {
        private bool disposed;
        private LiteDatabase _db;

        public SessionRepository()
        {
             _db = new LiteDatabase(@"SessionsDB.db");
        }

        public IEnumerable<GameSession> GetAll()
        {
            return _db.GetCollection<GameSession>().FindAll();
        }

        public GameSession Get(string userId)
        {
            return _db.GetCollection<GameSession>("sessions").FindAll().FirstOrDefault(x=>x.User.Id == userId);
        }

        public void Create(GameSession item)
        {
            try
            {
                _db.GetCollection<GameSession>("sessions").Insert(item);

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
           
        }

        public void Update(GameSession item)
        {
            _db.GetCollection<GameSession>("sessions").Update(item);
        }

        public void Delete(GameSession item)
        {
            _db.GetCollection<GameSession>("sessions").Delete(x=>x.Id == item.Id);
        }

        public void Save()
        {
           return;
        }

        public virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _db.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
