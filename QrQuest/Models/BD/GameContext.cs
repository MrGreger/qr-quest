﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace QrQuest.Models
{
    public class GameContext : IdentityDbContext<User>
    {
        public DbSet<Game> Games { get; set;}
        public DbSet<Quest> Quests { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<Update> Updates { get; set; }
        public DbSet<Addition> Additions { get; set; }

        public DbSet<Subscription> Subscriptions { get; set; }
        public DbSet<SubscriptionOrder> SubscriptionOrders { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            base.OnConfiguring(optionsBuilder);
            optionsBuilder.UseSqlite("Filename=gameDb.db");
               
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            builder.Entity<Quest>()
                .HasOne(q => q.Game)
                .WithMany(g => g.Quests)
                .OnDelete(DeleteBehavior.Cascade);
                
        }
    }
}
