﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QrQuest.Models
{
    public class Update
    {
        public int Id { get; set; }
        public string Version { get; set; }
        public DateTime Date { get; set; }
        public List<Addition> Additions { get; set; }

    }

    public class Addition
    {

        public int Id { get; set; }
        public string Class { get; set; }
        public string Description { get; set; }
        public string Color { get; set; }
    }
}
