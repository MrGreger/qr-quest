﻿using System;
using System.Linq;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;
using QrQuest.Models;

namespace QrQuest
{
    public class Startup
    {

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public async void ConfigureServices(IServiceCollection services)
        {
            // services.AddSingleton<IHostingEnvironment>(new HostingEnvironment());
            services.AddMvc();
            services.AddDbContext<GameContext>()
                .AddEntityFrameworkSqlite();

            services.AddIdentity<User, IdentityRole>(o =>
             {
                 o.Password.RequireNonAlphanumeric = false;
                 o.Password.RequireDigit = false;
                 o.Password.RequireUppercase = false;
             })
                .AddEntityFrameworkStores<GameContext>()
                .AddDefaultTokenProviders();


            services.ConfigureApplicationCookie(o =>
            {
                o.LoginPath = "/Home/AdminLog";
            });


            services.AddAuthentication()
                .AddJwtBearer("Bearer", o =>
                 {
                     o.SaveToken = true;

                     o.RequireHttpsMetadata = false;
                     o.TokenValidationParameters = new TokenValidationParameters
                     {
                         ValidateIssuer = true,
                         ValidIssuer = AuthOptions.ISSUER,

                         ValidateAudience = true,
                         ValidAudience = AuthOptions.AUDIENCE,

                         ValidateLifetime = true,

                         IssuerSigningKey = AuthOptions.GetSymmetricSecurityKey(),
                         ValidateIssuerSigningKey = true,
                     };
                 });



            #region bd_init




            using (GameContext db = new GameContext())
            {
                //List<Quest> gs = db.Quests.ToList();
                //db.Quests.RemoveRange(gs);
                //db.SaveChanges();
                //db.Database.EnsureDeleted();
                // db.Database.EnsureCreated();

                //try
                //{

                
                db.Database.GetPendingMigrations();
                db.Database.Migrate();


                if (db.Roles.Any() == false)
                {
                    RoleManager<IdentityRole> roleManager = services.BuildServiceProvider().CreateScope().ServiceProvider
                         .GetRequiredService<RoleManager<IdentityRole>>();
                    await roleManager.CreateAsync(new IdentityRole("Admin"));
                }

                UserManager<User> userManager = services.BuildServiceProvider().CreateScope().ServiceProvider
                    .GetRequiredService<UserManager<User>>();


                User g = await userManager.FindByEmailAsync("241000g@mail.ru");
                User d = await userManager.FindByEmailAsync("littleacegh@gmail.com");

                if (g == null)
                {
                    g = new User
                    {
                        Email = "241000g@mail.ru",
                        UserName = "Greg",
                        EmailConfirmed = true
                    };

                    await userManager.CreateAsync(g, "Gera39143");
                }

                if (g != null)
                {
                    if (!await userManager.IsInRoleAsync(g, "Admin"))
                    {
                        await userManager.AddToRoleAsync(g, "Admin");
                    }
                }

                if (d != null)
                {
                    if (!await userManager.IsInRoleAsync(d, "Admin"))
                    {
                        await userManager.AddToRoleAsync(d, "Admin");
                    }
                }

                if (db.Subscriptions.Any() == false)
                {
                    db.Subscriptions.Add(new Subscription
                    {
                        Cost = 0,
                        Time = TimeSpan.FromDays(365)
                    });
                    db.SaveChanges();
                }
                else
                {
                    var a = db.Subscriptions.FirstOrDefault(x => x.Id == 1);
                    a.Cost = 0;
                    db.Update(a);
                    db.SaveChanges();
                }


                //var emailServise = new EmailService();
                //emailServise.SendMessage("Yep!It works!", "All right", "MrGreger@yandex.ru");


                //User u = db.Users.FirstOrDefault(x => x.Email == "241000g@mail.ru");
                //u.SubscriptionEnd += TimeSpan.FromDays(50);

                //db.Users.Update(u);
                //db.SaveChanges();

                //}
                //catch (Exception e)
                //{
                //    Console.WriteLine(e);
                //    throw;
                //}


                //Game game = new Game
                //{
                //    FinishMessage = "Ура!Ты прошел!Приходи за подарком в 213!",
                //    Quests = new List<Quest>
                //    {
                //        new Quest
                //        {
                //            Address = "каб.206",
                //            Answer = "iMac",
                //            Question = "4 близнеца, за одним ответ..."
                //        },
                //        new Quest
                //        {
                //            Address = "каб.204",
                //            Answer = "4:20",
                //            Question = "Под огнеборцем прячется ответ..."
                //        },
                //        new Quest
                //        {
                //            Address = "комната 210",
                //            Answer = "QrQ",
                //            Question = "Ответ у желтой девы..."
                //        }


                //    }
                //};

                //db.Games.Add(game);
                //db.SaveChanges();

                //QRCodeGenerator qrGenerator = new QRCodeGenerator();
                //QRCodeData qrCodeData = null;
                //Game g = db.Games.Include(x => x.Quests).ToList()[0];
                //foreach (var quest in g.Quests)
                //{
                //    qrCodeData = qrGenerator.CreateQrCode(quest.Id.ToString(), QRCodeGenerator.ECCLevel.Q);
                //    QRCode qrCode = new QRCode(qrCodeData);
                //    Bitmap qrCodeImage = qrCode.GetGraphic(50);
                //    qrCodeImage.Save($"game_{g.Id}_quest_{quest.Id}.png");
                //}

                //using (StreamReader sr = new StreamReader("serialized.txt"))
                //{
                //    List<Game> gs = JsonConvert.DeserializeObject<List<Game>>(sr.ReadToEnd());
                //    db.Games.AddRange(gs);
                //    db.SaveChanges();
                ////}

                //var userManager = services.BuildServiceProvider().GetRequiredService<UserManager<User>>();

                //var r = await userManager.CreateAsync(new User { Email = "241000g@mail.ru", UserName = "Greg", Creator = true }, "Gera39143");
            }
            #endregion
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseAuthentication();

            app.UseStaticFiles();


            app.UseMvc(routes =>
            {
                routes.MapRoute(name: "default",
                    template: "{controller=Home}/{action=Index}");
            });
        }
    }
}
