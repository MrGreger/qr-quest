﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using QrQuest.Models;
using QrQuest.Models.ViewModels;

namespace QrQuest.Controllers
{
    public class HomeController : Controller
    {

        private readonly IHostingEnvironment _appEnvironment;
        private UserManager<User> _userManager;
        private SignInManager<User> _signInManager;
        private GameContext _db;


        public HomeController(IHostingEnvironment environment, UserManager<User> userManager, SignInManager<User> signInManager, GameContext context)
        {
            _appEnvironment = environment;
            _userManager = userManager;
            _signInManager = signInManager;
            _db = context;
        }

        [HttpGet]
        public IActionResult Index()
        {
            List<Update> updates = new List<Update>(_db.Updates.Include(x => x.Additions).ToList());
            updates.Reverse();
            ViewBag.Updates = updates;
            return View();
        }


        //TODO : перенести это все в аккаунт контроллер 
        [HttpGet]
        public async Task<IActionResult> AdminLog()
        {
            await HttpContext.SignOutAsync();
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> AdminLog(LoginViewModel model)
        {
            if (!ModelState.IsValid)
            {
                Response.StatusCode = 400;
                return RedirectToAction("AdminLog", "Home");
            }

            User u = await _userManager.FindByNameAsync(model.Login);


            if (u == null)
            {
                Response.StatusCode = 400;
                return RedirectToAction("AdminLog", "Home");
            }          
        

            if(await _userManager.IsInRoleAsync(u, "Admin") == false)
            {
                Response.StatusCode = 400;
                return RedirectToAction("AdminLog", "Home");
            }


            var result = await _signInManager.PasswordSignInAsync(model.Login, model.Password, true, false);

            if (result.Succeeded)
            {
                return RedirectToAction("Index", "Home");
            }


            Response.StatusCode = 400;
            return RedirectToAction("AdminLog", "Home");
        }

        public async Task<IActionResult> Logout()
        {
            await _signInManager.SignOutAsync();
            return RedirectToAction("Index");
        }

        [HttpPost]
        public async Task<IActionResult> Index(Update model)
        {
            if (!ModelState.IsValid)
            {
                Response.StatusCode = 400;
                return View(model);
            }

            _db.Add(new Update { Version = model.Version, Date = model.Date, Additions = model.Additions });
            List<Update> updates = new List<Update>(_db.Updates.Include(x => x.Additions).ToList());
            updates.Reverse();
            ViewBag.Updates = updates;
            await _db.SaveChangesAsync();
            return RedirectToAction("Index","Home");
        }
    }
}