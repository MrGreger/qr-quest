﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using QrQuest.Models;
using QrQuest.Models.ViewModels;


namespace QrQuest.Controllers
{
    [Produces("application/json")]
    [Route("api/Account")]
    public class AccountController : Controller
    {
        private UserManager<User> _userManager;
        private SignInManager<User> _signInManager;

        public AccountController(UserManager<User> userManager, SignInManager<User> signInManager)
        {
            _userManager = userManager;
            _signInManager = signInManager;
        }

        [HttpGet("/GetUserRating")]
        public async Task<int?> GetUserRating(string username)
        {
            if (username == null)
            {
                Response.StatusCode = 400;
                return null;
            }

            return (await _userManager.FindByNameAsync(username)).Rating;
        }

        [HttpPost("/token")]
        public async Task Token(string username, string password)
        {
            if (username == null || password == null)
            {
                Response.StatusCode = 400;
                return;
            }

            User u = await _userManager.FindByNameAsync(username);

            if (u == null || await _userManager.CheckPasswordAsync(u, password) == false)
            {
                Response.StatusCode = 400;
                await Response.WriteAsync("Invalid username or password.");
                return;
            }

            if (await _userManager.GetClaimsAsync(u) != null)
            {
                var res = await _userManager.RemoveClaimsAsync(u, await _userManager.GetClaimsAsync(u));
            }

            await _userManager.AddClaimAsync(u, new Claim(ClaimTypes.NameIdentifier, u.Id));
            await _userManager.AddClaimAsync(u, new Claim("username", username));

            foreach (var r in await _userManager.GetRolesAsync(u))
            {
                await _userManager.AddClaimAsync(u, new Claim(ClaimTypes.Role,r));
            }

            var curDate = DateTime.Now;

            var jwt = new JwtSecurityToken(AuthOptions.ISSUER,
                AuthOptions.AUDIENCE,
                await _userManager.GetClaimsAsync(u),
                curDate,
                curDate.Add(TimeSpan.FromMinutes(AuthOptions.LIFETIME)),
                new SigningCredentials(AuthOptions.GetSymmetricSecurityKey(),
                    SecurityAlgorithms.HmacSha256));

            var encodedJwt = new JwtSecurityTokenHandler().WriteToken(jwt);

            var response = new
            {
                access_token = encodedJwt,
                for_user = u.UserName,
                expire_date = curDate.Add(TimeSpan.FromMinutes(AuthOptions.LIFETIME)),
                is_creator = true
            };

            Console.WriteLine($"[{DateTime.Now}] {u.UserName} got token ");

            await Response.WriteAsync(JsonConvert.SerializeObject(response, new JsonSerializerSettings { Formatting = Formatting.Indented }));
        }

        [HttpPost("/Account/Register")]
        public async Task Register(RegisterViewModel model)
        {
            if (ModelState.IsValid)
            {
                User u = await _userManager.FindByEmailAsync(model.Email);

                if (u != null)
                {
                    Response.StatusCode = 400;
                    await Response.WriteAsync("Пользователь с таким Email уже зарегистрирован : " + u.Email);
                    return;
                }

                u = await _userManager.FindByNameAsync(model.Username);

                if (u != null)
                {
                    Response.StatusCode = 400;
                    await Response.WriteAsync("Пользователь с таким Логином уже зарегистрирован : " + u.UserName);
                    return;
                }

                u = new User { Email = model.Email, UserName = model.Username, Creator = true, Password = model.Password };

                if (model.Password == model.ConfirmPassword)
                {
                    var res = await _userManager.CreateAsync(u,
                         model.Password);


                    if (!res.Succeeded)
                    {
                        Response.StatusCode = 400;

                        string err = "";

                        foreach (var error in res.Errors)
                        {
                            err += error.Description + "\n";
                        }

                        await Response.WriteAsync(err);

                        return;
                    }

                }

                Console.WriteLine($"{u.UserName} added");

                Response.StatusCode = 200;
                EmailService email = new EmailService();
                email.SendMessage($"Спасибо за регистрацию! Ваши данные для входа:" +
                                  $"Логин: {model.Username} <br/>" +
                                  $"Пароль: {model.Password}",
                                  "Спасибо за регистраицю!",
                                  model.Email);
                await Response.WriteAsync("Пользователь зарегистрирован");

                return;
            }




            Response.StatusCode = 400;

            string ErrorMessage = "";

            foreach (var value in ModelState.Values)
            {
                foreach (var error in value.Errors)
                {
                    ErrorMessage += error.ErrorMessage + "\n";
                }
            }

            await Response.WriteAsync(ErrorMessage);
        }


    }
}