﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using QrQuest.Models;
using static System.Console;

namespace QrQuest.Controllers
{
    [Authorize(AuthenticationSchemes = "Bearer")]
    public class ShopController : Controller
    {
        private string merchID = "556941699";
        private string curr = "RUB";
        private string secret = "kQSoGdt0enJQQL2S";

        private GameContext _context;
        private UserManager<User> _userManager;
        public ShopController(GameContext context, UserManager<User> userManager)
        {
            _context = context;
            _userManager = userManager;
        }

        [Route("[controller]/Buy")]
        public async Task<ActionResult> Index(int gameId)
        {

            Game g = _context.Games.FirstOrDefault(x => x.Id == gameId);


            if (g == null)
            {
                return BadRequest();
            }

            if (!g.Cost.HasValue || g.Cost == 0)
            {
                return BadRequest("Игра бесплатна");
            }

            User u = await _userManager.FindByIdAsync(_userManager.GetUserId(User));

            if (u == null)
            {
                return BadRequest();
            }


            string url = "";

            Order o = _context.Orders.Include(t => t.Product).Include(y => y.Customer)
                .FirstOrDefault(x => x.Product.Id == g.Id && x.Customer.Id == u.Id);

            if (o != null && o.OrderState == OrderState.Ok)
            {
                return BadRequest("Игра уже куплена");
            }

            if (o != null && o.OrderState == OrderState.Canseled)
            {
                var toDelete = _context.Orders.FirstOrDefault(x => x.Id == o.Id);
                if (toDelete == null)
                {
                    return BadRequest();
                }
                _context.Orders.Remove(toDelete);
                _context.SaveChanges();
            }


            if (o != null && o.OrderState == OrderState.WaitingForPurchase)
            {
                string sCrc = hash($"{merchID}:{o.InvId}:{o.Sum:N2}:{curr}:{EncodeTo64("gam")}:{secret}").ToUpper();

                WriteLine($"{merchID}\n:{o.InvId}\n:{o.Sum:N2}\n:{curr}\n:{EncodeTo64("gam")}\n:{secret}");

                WriteLine();
                WriteLine($"crc: {sCrc}");

                //Уже есть заказ этой игры для этого юзера
                url =
                    "https://payeer.com/merchant/" +
                    $"?m_shop={merchID}&m_orderid={o.InvId}&m_amount={o.Sum:N2}&m_curr=RUB&m_desc={EncodeTo64("gam")}&m_sign={sCrc}&lang=ru";
                return Json(url);
            }


            //создание нового заказа

            int lastId = 0;
            if (_context.Orders.Any())
            {
                lastId = Int32.Parse(_context.Orders.Last().InvId);
            }

            Order order = new Order
            {
                Customer = u,
                InvId = (lastId + 1).ToString(),
                OrderState = OrderState.WaitingForPurchase,
                Product = g,
                Sum = g.Cost ?? 0
            };


            _context.Orders.Add(order);
            _context.SaveChanges();

            string crc = hash($"{merchID}:{order.InvId}:{(int)order.Sum}:{curr}:{EncodeTo64("gam")}:{secret}").ToUpper();
            //Уже есть заказ этой игры для этого юзера
            url =
                "https://payeer.com/merchant/" +
                $"?m_shop={merchID}&m_orderid={order.InvId}&m_amount={order.Sum:N2}&m_curr=RUB&m_desc={EncodeTo64("gam")}&m_sign={crc}&lang=ru";
            return Json(url);
        }

        [AllowAnonymous]
        [Route("[controller]/OnResult")]
        public async Task<IActionResult> OnResult(PurchaseViewModel model)
        {
            Console.WriteLine("go");
            if (model.m_desc == "gam")
            {
                Order order = _context.Orders.FirstOrDefault(x => x.InvId == model.m_orderid);

                if (order == null)
                {
                    return BadRequest();
                }


                if (order.OrderState == OrderState.Ok)
                {
                    return BadRequest("Заказ уже оплачен");
                }

                string sign = hash($"{model.m_operation_id}:{model.m_operation_ps}:{model.m_operation_date}:{model.m_operation_pay_date}:{model.m_shop}:{model.m_orderid}:{model.m_amount}:{model.m_curr}:{model.m_desc}:{model.m_status}");



                if (sign == model.m_sign && model.m_status == "success")
                {
                    order.OrderState = OrderState.Ok;

                    _context.Orders.Update(order);
                    _context.SaveChanges();

                    return Ok($"{model.m_orderid}|success");
                }

                return Ok($"{model.m_orderid}|error");
            }
            else
            {
                SubscriptionOrder order = _context.SubscriptionOrders.Include(y => y.Subscription).Include(z => z.Customer).FirstOrDefault(x => x.InvId == model.m_orderid);

                if (order == null)
                {
                    Console.WriteLine("order err");
                    return BadRequest();
                }


                if (order.OrderState == OrderState.Ok)
                {
                    return BadRequest("Заказ уже оплачен");
                }

                string sign = hash($"{model.m_operation_id}:{model.m_operation_ps}:{model.m_operation_date}:{model.m_operation_pay_date}:{model.m_shop}:{model.m_orderid}:{model.m_amount}:{model.m_curr}:{model.m_desc}:{model.m_status}");



                if (sign == model.m_sign && model.m_status == "success")
                {

                    User u = order.Customer;
                    u.SubscriptionEnd = DateTime.Now + order.Subscription.Time;

                    order.OrderState = OrderState.Ok;
                    _context.Users.Update(u);
                    _context.SubscriptionOrders.Update(order);
                    _context.SaveChanges();
                    return Ok($"{model.m_orderid}|success");
                }

                return Ok($"{model.m_orderid}|error");
            }
        }

        [Route("[controller]/Subscribe")]
        public async Task<ActionResult> Subscribe(int subscribeId)
        {
            //Subscription s = _context.Subscriptions.FirstOrDefault(x => x.Id == subscribeId);


            //if (s == null)
            //{
            //    return BadRequest();
            //}

            //User u = await _userManager.FindByIdAsync(_userManager.GetUserId(User));

            //if (u == null)
            //{
            //    return BadRequest();
            //}

            ////контрольная сумма для ссылки


            //string url = "";

            //if (u.SubscriptionEnd > DateTime.Today)
            //{
            //    return BadRequest("Подписка уже куплена");
            //}

            //SubscriptionOrder o = _context.SubscriptionOrders.Include(t => t.Subscription).Include(y => y.Customer)
            //    .FirstOrDefault(x => x.Subscription.Id == s.Id && x.Customer.Id == u.Id);

            //if (o != null && o.OrderState == OrderState.Canseled)
            //{
            //    var toDelete = _context.SubscriptionOrders.FirstOrDefault(x => x.Id == o.Id);
            //    if (toDelete == null)
            //    {
            //        return BadRequest();
            //    }
            //    _context.SubscriptionOrders.Remove(toDelete);
            //    _context.SaveChanges();
            //}

            //MD5CryptoServiceProvider md5 = new MD5CryptoServiceProvider();
            //StringBuilder sbSignature = new StringBuilder();
            //byte[] bSignature;
            //string sCrc;
            //if (o != null && o.OrderState == OrderState.WaitingForPurchase)
            //{
            //    md5 = new MD5CryptoServiceProvider();
            //    sbSignature = new StringBuilder();
            //    bSignature = md5.ComputeHash(Encoding.ASCII.GetBytes("72788:" + s.Cost + ":" + secret1 + ":" + o.InvId));
            //    foreach (byte b in bSignature)
            //        sbSignature.AppendFormat("{0:x2}", b);

            //    sCrc = sbSignature.ToString();
            //    //Уже есть заказ этой игры для этого юзера
            //    url =
            //        "http://www.free-kassa.ru/merchant/cash.php?"
            //        + $"m=72788&oa={s.Cost}&o={o.InvId}&s={sCrc}&lang=ru&i=&em={o.Customer.Email}&us_type=sub";
            //    return Json(url);
            //}


            ////создание нового заказа

            //    int lastId = 0;
            //    if (_context.SubscriptionOrders.Any())
            //    {
            //        lastId = Int32.Parse(_context.SubscriptionOrders.Last().InvId);
            //    }

            //    SubscriptionOrder order = new SubscriptionOrder
            //    {
            //        Customer = u,
            //        InvId = (lastId + 1).ToString(),
            //        OrderState = OrderState.WaitingForPurchase,
            //        Subscription = s,
            //    };


            //    _context.SubscriptionOrders.Add(order);
            //    _context.SaveChanges();
            //try
            //{
            //    md5 = new MD5CryptoServiceProvider();
            //    sbSignature = new StringBuilder();
            //    bSignature = md5.ComputeHash(Encoding.ASCII.GetBytes("72788:" + s.Cost + ":" + secret1 + ":" + order.InvId));
            //    foreach (byte b in bSignature)
            //        sbSignature.AppendFormat("{0:x2}", b);

            //    sCrc = sbSignature.ToString();
            //    url =
            //        "http://www.free-kassa.ru/merchant/cash.php?"
            //        + $"m=72788&oa={s.Cost}&o={order.InvId}&s={sCrc}&lang=ru&i=&em={order.Customer.Email}&us_type=sub";

            //    return Json(url);
            //}
            //catch (Exception e)
            //{
            //    Console.WriteLine(e);
            //    throw;
            //}

            return null;
        }

        private string hash(string text)
        {
            byte[] data = Encoding.Default.GetBytes(text);
            var result = new SHA256Managed().ComputeHash(data);
            return BitConverter.ToString(result).Replace("-", "").ToLower();
        }

        string EncodeTo64(string toEncode)

        {

            byte[] toEncodeAsBytes

                = System.Text.ASCIIEncoding.ASCII.GetBytes(toEncode);

            string returnValue

                = System.Convert.ToBase64String(toEncodeAsBytes);

            WriteLine("base64: "+returnValue);

            return returnValue;

        }

    }

    public class PurchaseViewModel
    {
        public string m_operation_id { get; set; }
        public string m_sign { get; set; }
        public string m_operation_ps { get; set; }
        public string m_operation_date { get; set; }
        public string m_operation_pay_date { get; set; }
        public string m_shop { get; set; }
        public string m_orderid { get; set; }
        public string m_amount { get; set; }
        public string m_curr { get; set; }
        public string m_desc { get; set; }
        public string m_status { get; set; }
    }
}