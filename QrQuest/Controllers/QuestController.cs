﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using QrQuest.Models;
using QrQuest.Models.ViewModels;

namespace QrQuest.Controllers
{
    [Authorize(AuthenticationSchemes = "Bearer")]
    [Produces("application/json")]
    [Route("api/Quest")]
    public class QuestController : Controller
    {
        private GameContext _db { get; set; }
        private SessionRepository _sessionDb { get; set; }
        private UserManager<User> _userManager;

        public QuestController(GameContext db, UserManager<User> userManager)
        {
            _db = db;
            _sessionDb = new SessionRepository();
            _userManager = userManager;
        }

        [HttpGet("GetQuestLocation")]
        public async Task<Point?> GetQuestLocation()
        {

            var uid = _userManager.GetUserId(User);
            User u = await _userManager.FindByIdAsync(uid);


            if (u == null)
            {
                return null;
            }

            GameSession session = _sessionDb.Get(u.Id);

            if (session == null)
            {
                return null;
            }

            Game game = session.Game;

            if (game.Cost.HasValue && game.Cost.Value > 0)
            {
                Order o = _db.Orders.Include(x => x.Customer).FirstOrDefault(x => x.Customer.Id == u.Id);

                if (o == null || o.OrderState != OrderState.Ok)
                {
                    return null;
                }
            }


            Quest q = _db.Quests.Include(c => c.Location).FirstOrDefault(x => x.Id == session.Game.Quests[session.CurrentQuest].Id);

            Point p = new Point();

            if (q.Address != null)
            {
                p.Address = q.Address;
            }

            if (q.Location != null)
            {
                p.Location = q.Location;
            }

            p.GameId = game.Id;

            return p;
        }

        [HttpGet("Quests/{id}")]
        public async Task<QuestViewModel> GetQuest(int id)
        {
            User u = await _userManager.FindByIdAsync(_userManager.GetUserId(User));

            if (u == null)
            {
                return null;
            }

            GameSession session = _sessionDb.Get(u.Id);

            if (session == null)
            {
                return null;
            }

            Quest curQuest = session.Game.Quests[session.CurrentQuest];

            if (curQuest == null)
            {
                return null;
            }

            if (curQuest.Id != id)
            {
                return null;
            }

            return new QuestViewModel { Question = curQuest.Question, Id = curQuest.Id };

        }


        [Authorize(AuthenticationSchemes = "Bearer")]
        [HttpGet("CheckQuest/{id}")]
        public async Task<QuestAnswerValidationResponse?> CheckQuest(int? id, string answer)
        {

            string a = User.Claims.FirstOrDefault(x => x.Type == "username").Value;

            if (id == null || answer == null)
            {
                return null;
            }

            Quest questToCheck = _db.Quests.FirstOrDefault(x => x.Id == id);
            if (questToCheck == null)
            {
                return null;
            }

            //Если ответ неверный
            if (questToCheck.Answer.Trim() != answer.Trim())
            {
                return new QuestAnswerValidationResponse { ValidAnswer = false };
            }

            Game curGame = _db.Games.Include(i => i.Quests).ThenInclude(c => c.Location).ToList().FirstOrDefault(x => x.Quests.Contains(questToCheck));

            if (curGame == null)
            {
                return null;
            }

            int questToCheckIndex = curGame.Quests.OrderBy(i => i.Id).ToList().IndexOf(curGame.Quests.FirstOrDefault(x => x.Id == questToCheck.Id));


            //Игра окончена
            if (questToCheckIndex == curGame.Quests.Count - 1)
            {
                User u = await _userManager.FindByIdAsync(_userManager.GetUserId(User));

                GameSession session = _sessionDb.Get(u.Id);

                if (session == null)
                {
                    Response.StatusCode = 400;
                    await Response.WriteAsync("Данная игровая сессия уже завершена");
                    return null;
                }

                _sessionDb.Delete(session);

                u.Rating += curGame.AdditionalRating.Value;

                _db.Users.Update(u);
                _db.SaveChanges();

                return new QuestAnswerValidationResponse { IsFinish = true, Message = curGame.FinishMessage };
            }

            int nextQuestId = questToCheckIndex + 1;
            Quest nextQuest = curGame.Quests[nextQuestId];

            if (nextQuest == null)
            {
                return null;
            }

            GameSession s = _sessionDb.Get(_userManager.GetUserId(User));
            s.CurrentQuest = nextQuestId;

            _sessionDb.Update(s);

            return new QuestAnswerValidationResponse
            {
                IsFinish = false,
                ValidAnswer = true,
                Message = null
            };
        }


    }
}