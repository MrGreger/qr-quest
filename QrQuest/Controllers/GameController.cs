﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using QrQuest.Models;
using QrQuest.Models.ViewModels;

namespace QrQuest.Controllers
{

    [Authorize(AuthenticationSchemes = "Bearer")]
    [Route("api/[controller]")]
    public class GameController : Controller
    {
        private static Random _rand;
        private GameContext _db { get; set; }
        private SessionRepository _sessionDb;
        private UserManager<User> _userManager;

        public GameController(GameContext context, UserManager<User> userManager)
        {
            _db = context;
            _sessionDb = new SessionRepository();
            _userManager = userManager;

            if (_rand == null)
            {
                _rand = new Random();
            }
        }

        [HttpGet]
        [Route("StartGame/{id}")]
        public async Task<IActionResult> StartGame(int id)
        {

            Game game = null;

            foreach (var item in _db.Games.Include(x => x.Quests))
            {
                if (item.Id == id)
                {
                    game = item;
                    break;
                }
            }

            if (game == null)
            {
                // Response.StatusCode = 400;
                return BadRequest();
            }

            User u = await _userManager.FindByIdAsync(_userManager.GetUserId(User));

            if (u == null)
            {
                return BadRequest();
            }

            //if (game.Cost.HasValue && game.Cost.Value > 0)
            //{
            //    Order o = _db.Orders.Include(y=>y.Customer).Include(z=>z.Product).FirstOrDefault(x => x.Customer.Id == u.Id && x.Product.Id == game.Id);
            //    if (o == null || o.OrderState != OrderState.Ok)
            //    {
            //        return BadRequest("Вы должны купить эту игру");
            //    }
            //}

            GameSession session = new GameSession { Game = game, User = u, CurrentQuest = 0 };
            if (_sessionDb.Get(u.Id) != null)
            {
                _sessionDb.Delete(_sessionDb.Get(u.Id));
            }

            _sessionDb.Create(session);

            return Ok();
        }

        [HttpGet]
        [Route("GetGameByQuest/{id}")]
        public async Task<IActionResult> GetGameByQuest(int id)
        {

            Game game = null;

            game = _db.Games.Include(x => x.Quests).FirstOrDefault(y => y.Quests.FirstOrDefault(i => i.Id == id) != null);


            if (game == null)
            {
                // Response.StatusCode = 400;
                return BadRequest("Игры не существует");
            }

            User u = await _userManager.FindByIdAsync(_userManager.GetUserId(User));

            if (u == null)
            {
                return BadRequest();
            }

            var result = new GameViewModel
            {
                Id = game.Id,
                Name = game.Name,
                Description = game.Description,
                Bought = true,
                Cost = game.Cost ?? 0,
                StartPoint = game.Quests.First().Address

            };

            if (game.Cost.HasValue && game.Cost.Value > 0)
            {
                Order o = _db.Orders.Include(y => y.Customer).Include(z => z.Product).FirstOrDefault(x => x.Customer.Id == u.Id && x.Product.Id == game.Id);
                if (o == null || o.OrderState != OrderState.Ok)
                {
                    result.Bought = false;
                }
            }

            return Ok(result);
        }

        [HttpGet]
        [Route("GetGameByCode/{code}")]
        public async Task<GameViewModel> GetGameByCode(string code)
        {
            Game game = _db.Games.Include(x => x.Quests).ThenInclude(c => c.Location).SingleOrDefault(z => z.AccessCode == code);

           return new GameViewModel(game.Id, game.Name, game.Description, game.Quests[0].Address, true, 0);
        }

        [HttpGet]
        [Route("GetGames")]
        public async Task<List<GameViewModel>> GetGames()
        {
            List<Game> games = _db.Games.Include(x => x.Quests).ThenInclude(c => c.Location)
                                                               .Where(z => z.AccessByCode == false)
                                                               .Where(x => x.Quests.Any()).ToList();
            if (games.Count == 0)
            {
                return null;
            }
            List<GameViewModel> gamesModel = new List<GameViewModel>();

            //List<Order> orders = _db.Orders.Include(x => x.Product).Include(y => y.Customer).ToList();

            User user = await _userManager.FindByIdAsync(_userManager.GetUserId(User));

            if (user == null)
            {
                Response.StatusCode = 400;
                return null;
            }

            try
            {

                foreach (var game in games)
                {
                    //if (game.Quests.Count == 0)
                    //{
                    //    continue;
                    //}

                    GameViewModel model = new GameViewModel(game.Id, game.Name, game.Description, game.Quests[0].Address, true, 0);

                    //Order o = orders.FirstOrDefault(x => x.Customer.Id == user.Id && x.Product.Id == game.Id);
                    //if (o != null)
                    //{
                    //    model.Bought = o.OrderState == OrderState.Ok;
                    //}

                    //if (model.Cost == 0)
                    //{
                    model.Bought = true;
                    //}

                    gamesModel.Add(model);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }


            return gamesModel;
        }

        [HttpGet]
        [Route("GetCreatedGames")]
        public async Task<List<Game>> GetUserGames()
        {
            User u = await _userManager.FindByIdAsync(_userManager.GetUserId(User));
            return _db.Games.Include(i => i.Quests).ThenInclude(c => c.Location).Where(x => x.Creator == u).ToList();
        }

        [HttpPost]
        [Route("CreateGame")]
        public async Task<Game> CreateGame([FromBody]Game game)
        {
            if (ModelState.IsValid == false)
            {
                return null;
            }

            try
            {
                User u = await _userManager.FindByIdAsync(_userManager.GetUserId(User));

                //if (u.Creator == false)
                //{
                //    Response.StatusCode = 401;
                //    return null;
                //}

                game.AccessCode = GenrateAccessCode(game);


                game.Creator = u;
                _db.Games.Add(game);
                _db.SaveChanges();
                return game;
            }
            catch (Exception e)
            {
                return null;
            }
        }

        [HttpDelete]
        [Route("DeleteGame/{id}")]
        public async Task DeleteGame(int id)
        {
            try
            {
                User u = await _userManager.FindByIdAsync(_userManager.GetUserId(User));

                //if (u.Creator == false)
                //{
                //    Response.StatusCode = 401;
                //    return;
                //}

                Game game = _db.Games.Include(x => x.Quests).FirstOrDefault(i => i.Id == id);

                if (game == null)
                {
                    Response.StatusCode = 400;
                    await Response.WriteAsync("игры не существет");
                    return;
                }
                if (game.Creator != u)
                {
                    Response.StatusCode = 400;
                    await Response.WriteAsync("Вы не создатель этого квеста");
                    return;
                }


                _db.Orders.RemoveRange(_db.Orders.Where(o => o.Product.Id == game.Id));

                _db.Games.Remove(game);

                _db.SaveChanges();

                var a = _db.Quests;

                var games = _sessionDb.GetAll();
                if (games != null)
                {
                    var toDelete = games.Where(x => x.Game.Id == game.Id);
                    if (toDelete != null)
                    {
                        foreach (var session in toDelete)
                        {
                            _sessionDb.Delete(session);
                        }
                    }
                }

                Response.StatusCode = 200;
                return;

            }
            catch (Exception e)
            {
                Response.StatusCode = 400;
                await Response.WriteAsync(e.Message);
                Console.WriteLine(e.Message);
            }
        }

        [HttpPut]
        [Route("UpdateGame")]
        public async Task UpdateGame([FromBody]Game game)
        {
            try
            {
                User u = await _userManager.FindByIdAsync(_userManager.GetUserId(User));

                //if (u.Creator == false)
                //{
                //    Response.StatusCode = 401;
                //    return;
                //}
                _db.Quests.Include(x => x.Location).Load();
                Game gameToUpdate = _db.Games.Include(x => x.Quests).Include(y => y.Creator).FirstOrDefault(i => i.Id == game.Id);

                if (gameToUpdate == null)
                {
                    Response.StatusCode = 400;
                    return;
                }

                //Обновляем квесты

                int index = 0;

                while (index < game.Quests.Count)
                {
                    if (index < gameToUpdate.Quests.Count)
                    {
                        gameToUpdate.Quests[index].Address = game.Quests[index].Address;
                        gameToUpdate.Quests[index].Answer = game.Quests[index].Answer;
                        gameToUpdate.Quests[index].Question = game.Quests[index].Question;

                        if (gameToUpdate.Quests[index].Location != null && game.Quests[index].Location != null)
                        {
                            gameToUpdate.Quests[index].Location.Lat = game.Quests[index].Location.Lat;
                            gameToUpdate.Quests[index].Location.Lon = game.Quests[index].Location.Lon;
                        }
                        else
                        {
                            gameToUpdate.Quests[index].Location = game.Quests[index].Location;
                        }
                    }
                    else
                    {
                        gameToUpdate.Quests.Add(game.Quests[index]);
                    }
                    index++;
                }

                if (gameToUpdate.Quests.Count > game.Quests.Count)
                {
                    gameToUpdate.Quests.RemoveRange(game.Quests.Count, gameToUpdate.Quests.Count - game.Quests.Count);
                }
                gameToUpdate.Quests = gameToUpdate.Quests.OrderBy(x=>x.Id).ToList();


                //Обновлем параметры игры
                gameToUpdate.Description = game.Description;
                gameToUpdate.FinishMessage = game.FinishMessage;
                gameToUpdate.Name = game.Name;
                //gameToUpdate.AdditionalRating = game.AdditionalRating;
                //gameToUpdate.Cost = game.Cost;
                gameToUpdate.AccessByCode = game.AccessByCode;

                if (string.IsNullOrWhiteSpace(gameToUpdate.AccessCode))
                {
                    gameToUpdate.AccessCode = GenrateAccessCode(gameToUpdate);
                }

                if (gameToUpdate.Creator != u)
                {
                    Response.StatusCode = 400;
                    return;
                }

                _db.Games.Update(gameToUpdate);
                await _db.SaveChangesAsync();

                //Сбрасываем сессии этой игры
                var games = _sessionDb.GetAll();
                var toDelete = games.Where(x => x.Game.Id == game.Id);

                foreach (var session in toDelete)
                {
                    _sessionDb.Delete(session);
                }

                Response.StatusCode = 200;

            }
            catch (Exception e)
            {
                Response.StatusCode = 400;
            }

        }

        private string GenrateAccessCode(Game g)
        {
            string allowedChars = "qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM1234567890";
            string code;
            do
            {
                code = "";
                for (int i = 0; i < 10; i++)
                {
                    code += allowedChars[_rand.Next(0, allowedChars.Length)];
                }
            } while (_db.Games.SingleOrDefault(x => x.AccessCode == code) != null);

            return code;
        }
    }
}