﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace QrQuest.Migrations
{
    public partial class AccessCodes : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "AccessByCode",
                table: "Games",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<string>(
                name: "AccessCode",
                table: "Games",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "AccessByCode",
                table: "Games");

            migrationBuilder.DropColumn(
                name: "AccessCode",
                table: "Games");
        }
    }
}
