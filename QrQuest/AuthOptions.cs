﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.IdentityModel.Tokens;

namespace QrQuest
{
    public class AuthOptions
    {
        public const string ISSUER = "QuestServer"; // издатель токена
        public static string AUDIENCE { get; private set; } = "92.53.119.154"; // потребитель токена
        const string KEY = "6913b4eadabcd2d1bcda6cc7941b91f2";   // ключ для шифрации
        public const int LIFETIME = 10; // время жизни токена - 1 минута
        public static SymmetricSecurityKey GetSymmetricSecurityKey()
        {
            return new SymmetricSecurityKey(Encoding.ASCII.GetBytes(KEY));
        }
    }
}
