﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Server.Kestrel.Core;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace QrQuest
{
    public class Program
    {

        public static string LISTEN_IP { get; private set; } = "127.0.0.1";

        public static void Main(string[] args)
        {
            ////#if RELEASE
            String strHostName = string.Empty;
            strHostName = Dns.GetHostName();
            IPHostEntry ipEntry = Dns.GetHostEntry(strHostName);
            IPAddress[] addr = ipEntry.AddressList;
            ////_listenIP = addr[2].ToString();
            string toConnect = addr.FirstOrDefault(x => x.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork).ToString();
            LISTEN_IP = toConnect;
            ////#endif
            ////#if DEBUG
            ////Console.WriteLine("hi");
            //LISTEN_IP = "127.0.0.1";
            ////#endif

            Console.ForegroundColor = ConsoleColor.DarkCyan;
            Console.WriteLine("Вы можете подключиться к вашему серверу с любого устройства,\n подключенного к локальной сети данного устройства (по WIFI/Lan) по IP адресу : " + toConnect + ":5050");
            Console.WriteLine("Чтобы подключитья из админке к серверу, откройте в папке админки файл 'QrQuestAdminApp.exe.config' (в блокноте)");
            Console.WriteLine("И измените value строки  <add key=\"ServerIp\" value=\"\"/>  на " + toConnect + ":5050");

            BuildWebHost(args).Run();
        }

        public static IWebHost BuildWebHost(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseStartup<Startup>()
                .UseKestrel(options =>
                {
                    options.Limits.MaxConcurrentConnections = 100;
                    options.Limits.MaxRequestBodySize = 10 * 1024;
                    options.Limits.MinRequestBodyDataRate =
                        new MinDataRate(bytesPerSecond: 100, gracePeriod: TimeSpan.FromSeconds(10));
                    options.Limits.MinResponseDataRate =
                        new MinDataRate(bytesPerSecond: 100, gracePeriod: TimeSpan.FromSeconds(10));
                    options.Listen(IPAddress.Parse(LISTEN_IP), port: 5050);
                })
                .Build();
    }
}
