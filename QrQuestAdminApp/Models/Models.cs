﻿using Newtonsoft.Json;
using Prism.Mvvm;
using QrQuestAdminApp.ViewModels;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace QrQuestAdminApp.Models
{
    public class Game : BindableBase
    {
        public int Id { get; set; }


        private string _title;
        public string Title
        {
            get { return _title; }
            set
            {
                _title = value;
                _title = _title.Replace(Environment.NewLine, " ").Substring(0, _title.Length >= 10 ? 10 : _title.Length);
                RaisePropertyChanged("Title");             
            }
        }

        private string _name;
        public string Name
        {
            get { return _name; }
            set
            {
                _name = value;
                Title = _name;
                RaisePropertyChanged("Name");
                ApplicationViewModel.Instanse.CreateGameDelegate.RaiseCanExecuteChanged();
                ApplicationViewModel.Instanse.UpdateGameDelegate.RaiseCanExecuteChanged();
            }
        }


        public string Description { get; set; }
        public int AdditionalRating { get; set; }
        public ObservableCollection<Quest> Quests { get; set; }

        public string FinishMessage { get; set; }
        public decimal? Cost { get; set; }

        private bool _accessByCode;
        public bool AccessByCode
        {
            get
            {
                return _accessByCode;
            }
            set
            {
                _accessByCode = value;
                RaisePropertyChanged("AccessByCode");
            }
        }
        public string AccessCode { get; set; }
    }

    public class Quest : BindableBase
    {
        private string _title;
        [JsonIgnore]
        public string Title
        {
            get
            {
                return _title;
            }
            set
            {
                _title = value;
                _title = _title.Replace(Environment.NewLine, " ").Substring(0, Question.Length >= 10 ? 10 : Question.Length);
                RaisePropertyChanged("Title");

            }
        }

        private string _question;

        public int Id { get; set; }
        public string Question
        {
            get
            {
                return _question;
            }
            set
            {
                _question = value;
                Title = value;
                RaisePropertyChanged("Question");
            }
        }
        public string Answer { get; set; }
        public string Address { get; set; }

        private Location _location;
        public Location Location
        {
            get { return _location; }
            set
            {
                _location = value;
                RaisePropertyChanged("Location");
            }
        }
    }


    public class Location : BindableBase
    {
        public int Id;
        private double _lon;
        private double _lat;

        public double Lon
        {
            get { return _lon; }
            set
            {
                _lon = value;
                RaisePropertyChanged("Lon");
                RaisePropertyChanged("AsString");
            }
        }
        public double Lat
        {
            get { return _lat; }
            set
            {
                _lat = value;
                RaisePropertyChanged("Lat");
                RaisePropertyChanged("AsString");
            }
        }

        [JsonIgnore]
        public string AsString
        {
            get { return $"{Lat.ToString().Replace(',', '.')},{Lon.ToString().Replace(',', '.')}"; }
        }
    }
}
