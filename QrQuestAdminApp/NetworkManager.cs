﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using Newtonsoft.Json;
using QrQuestAdminApp.Models;
using QrQuestAdminApp.Pages;

namespace QrQuestAdminApp
{
    public class NetworkManager
    {
        public static string _username { get; private set; }
        private static string _password;

        public readonly string SERVERIP = "http://192.168.0.109:5000";

        public NetworkManager()
        {
            SERVERIP = ConfigurationManager.AppSettings.Get("ServerIp");
        }

        private static string _token;
        private DateTime _expireDate;

        public async Task<bool> GetToken(string username, string password)
        {
            if (_expireDate > DateTime.Now)
            {
                return true;
            }

            try
            {
                HttpClient client = new HttpClient();
                HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Post, $"{SERVERIP}/token?username={username}&&password={password}");
                var response = await client.SendAsync(request);


                bool succeded = response.StatusCode == (HttpStatusCode)200 ? true : false;

                if (!succeded)
                {
                    return false;
                }

                Dictionary<string, string> responseDictionary =
                    JsonConvert.DeserializeObject<Dictionary<string, string>>(
                        await response.Content.ReadAsStringAsync());

                var tok = responseDictionary["access_token"];
                bool creator = false;

                if (responseDictionary.ContainsKey("is_creator"))
                {
                    creator = Boolean.Parse(responseDictionary["is_creator"]);
                }

                if (responseDictionary.ContainsKey("expire_date"))
                {
                    _expireDate = DateTime.Parse(responseDictionary["expire_date"]);
                }

                if (tok != null)
                {
                    _token = tok;
                    _username = username;
                    _password = password;
                }
                else
                {
                    return false;
                }

                if (!creator)
                {
                    if (MessageBox.Show(
                            "На вашем аккаунте не преобретен доступ к созданию квестов!Перейти на страницу оплаты?", "",
                            MessageBoxButton.YesNo) == MessageBoxResult.No)
                    {
                        return false;
                    }
                    else
                    {

                        HttpClient cl = new HttpClient();
                        HttpRequestMessage rq = new HttpRequestMessage(HttpMethod.Get, "http://qrquestct.ru/Shop/Subscribe?subscribeId=1");
                        rq.Headers.Add("Authorization", $"Bearer {_token}");

                        var r = await cl.SendAsync(rq);

                        Process.Start(JsonConvert.DeserializeObject<string>(await r.Content.ReadAsStringAsync()));


                        return false;
                    }
                }

                if (_token != null && creator)
                {


                    return true;
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
                Console.WriteLine(e);
            }

            return false;
        }

        public async Task<Game> CreateGame(Game game)
        {

            string serialized = JsonConvert.SerializeObject(game);

            var response = await SendRequestAsync(HttpMethod.Post, $"{SERVERIP}/api/Game/CreateGame", new StringContent(serialized, Encoding.UTF8, "application/json"));

            Game g;

            try
            {
                g = JsonConvert.DeserializeObject<Game>(await response.Content.ReadAsStringAsync());
                return g;
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public async Task<List<Game>> GetGames()
        {
            var response = await SendRequestAsync(HttpMethod.Get, $"{SERVERIP}/api/Game/GetCreatedGames");

            List<Game> g;
            try
            {
                g = JsonConvert.DeserializeObject<List<Game>>(await response.Content.ReadAsStringAsync());
                return g;
            }
            catch (Exception e)
            {
                MessageBox.Show("Ошибка при получении игр");
                return null;
            }
        }

        public async Task<bool> DeleteGame(int id)
        {


            var response = await SendRequestAsync(HttpMethod.Delete, $"{SERVERIP}/api/Game/DeleteGame/{id}");

            if (response.StatusCode == (HttpStatusCode)200)
            {
                MessageBox.Show("Объект удален");
                return true;
            }
            else
            {
                MessageBox.Show($"Ошибка удаления (status code {response.StatusCode})");
                return false;
            }
        }

        public async Task<bool> UpdateGame(Game game)
        {


            string serialized = JsonConvert.SerializeObject(game);

            var response = await SendRequestAsync(HttpMethod.Put, $"{SERVERIP}/api/Game/UpdateGame",
                new StringContent(serialized, Encoding.UTF8, "application/json"));

            if (response != null && response.StatusCode == (HttpStatusCode)200)
            {
                MessageBox.Show("Игра обновлена");
                return true;
            }
            else
            {
                MessageBox.Show($"Ошибка обновления (status code {response?.StatusCode})");
                return false;
            }
        }


        public async Task<bool> Register(RegisterViewModel model)
        {
            HttpClient client = new HttpClient();

            var result = await client.PostAsync($"{SERVERIP}/Account/Register", new FormUrlEncodedContent(new KeyValuePair<string, string>[]
            {
                new KeyValuePair<string, string>("Email",model.Email),
                new KeyValuePair<string, string>("Username",model.Username),
                new KeyValuePair<string, string>("Password",model.Password),
                new KeyValuePair<string, string>("ConfirmPassword",model.ConfirmPassword)
            }));

            if (result.StatusCode == (HttpStatusCode)200)
            {
                _username = model.Username;
                _password = model.Password;

                return (true);
            }
            else
            {
                MessageBox.Show(await result.Content.ReadAsStringAsync());
                return (false);
            }
        }

        private async Task<HttpResponseMessage> SendRequestAsync(HttpMethod method, string url, HttpContent content = null)
        {
            try
            {


                HttpResponseMessage result = null;

                if (await GetToken(_username, _password))
                {
                    HttpClient client = new HttpClient();
                    HttpRequestMessage request = new HttpRequestMessage(method, url);
                    request.Headers.Add("Authorization", $"Bearer {_token}");

                    if (content != null)
                    {
                        request.Content = content;
                    }

                    result = await client.SendAsync(request);

                }

                return result;
            }
            catch (Exception)
            {

                MessageBox.Show("Сервер недоступен");
            }

            return null;
        }
    }
}
