﻿using MaterialDesignThemes.Wpf;
using Newtonsoft.Json;
using Prism.Commands;
using Prism.Mvvm;
using QRCoder;
using QrQuestAdminApp.Models;
using QrQuestAdminApp.Pages;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Markup;

namespace QrQuestAdminApp.ViewModels
{
    public class ApplicationViewModel : BindableBase
    {
        public static ApplicationViewModel Instanse { get; protected set; }

        private Game _newGame;
        private Game _selectedGame;
        private NetworkManager _networkManager;

        public ObservableCollection<Game> CreatedGames { get; set; }
        public DelegateCommand<Game> CreateQuestDelegate { get; private set; }
        public DelegateCommand<Quest> DeleteQuestDelegate { get; private set; }
        public DelegateCommand UpdateGameDelegate { get; private set; }
        public DelegateCommand DeleteGameDelegate { get; private set; }
        public DelegateCommand RefreshGamesDelegate { get; private set; }
        public DelegateCommand CreateGameDelegate { get; private set; }
        public DelegateCommand<Game> GenerateQrCodesDelegate { get; private set; }
        public DelegateCommand<Quest> CreateLocationDelegate { get; private set; }
        public DelegateCommand<RoutedEventArgs> LoadedDelegate { get; private set;  }

        private IPageContol _currentPage;
        public IPageContol CurrentPage
        {
            get { return _currentPage; }
            set
            {
                DrawerHost.CloseDrawerCommand.Execute(null,null);
                _currentPage = value;
                RaisePropertyChanged("CurrentPage");
            }
        }

        private bool _redactFieldsVisible;
        public bool RedactFieldsVisible
        {
            get { return _redactFieldsVisible; }
            set
            {
                _redactFieldsVisible = value;
                RaisePropertyChanged("RedactFieldsVisible");
            }
        }

        public List<IPageContol> Pages { get; set; }

        public Game NewGame
        {
            get { return _newGame; }
            set
            {
                _newGame = value;
                RaisePropertyChanged("NewGame");
            }
        }

        public Game SelectedGame
        {
            get { return _selectedGame; }
            set
            {
                _selectedGame = value;

                RedactFieldsVisible = _selectedGame != null;

                UpdateGameDelegate.RaiseCanExecuteChanged();
                GenerateQrCodesDelegate.RaiseCanExecuteChanged();
                DeleteGameDelegate.RaiseCanExecuteChanged();

                RaisePropertyChanged("SelectedGame");
            }
        }

        public ApplicationViewModel()
        {
            if (Instanse == null)
            {
                Instanse = this;
            }


            CreateQuestDelegate = new DelegateCommand<Game>(CreateQuest);
            DeleteQuestDelegate = new DelegateCommand<Quest>(DeleteQuest);
            UpdateGameDelegate = new DelegateCommand(SaveGame, CanUpdateGame);
            DeleteGameDelegate = new DelegateCommand(DeleteGame, CanOperate);
            RefreshGamesDelegate = new DelegateCommand(RefreshGames);
            CreateGameDelegate = new DelegateCommand(CreateGame, CanCreateGame);
            GenerateQrCodesDelegate = new DelegateCommand<Game>(GenerateQrCodes, CanOperate);
            CreateLocationDelegate = new DelegateCommand<Quest>(CreateLocation);
            LoadedDelegate = new DelegateCommand<RoutedEventArgs>(DialogHost_Loaded);

            Pages = new List<IPageContol> { new CreateQuestPage { DataContext = this }, new RedactQuestPage { DataContext = this } };
        }

        private async Task<bool> TryAuth()
        {
            var result = (bool)await DialogHost.Show(new AuthDialog());
            return result;
        }


        private  void Init()
        {
            _networkManager = new NetworkManager();

            CreatedGames = new ObservableCollection<Game>();

            NewGame = new Game { Quests = new ObservableCollection<Quest>() };

            RefreshGamesDelegate.Execute();
            SelectedGame = CreatedGames.FirstOrDefault();

            CurrentPage = Pages[0];
        }

        private void CreateQuest(Game game)
        {
            var q = new Quest();
            game.Quests.Add(q);
        }

        private void DeleteQuest(Quest quest)
        {
            if (SelectedGame != null && SelectedGame.Quests.Contains(quest))
            {
                SelectedGame.Quests.Remove(quest);
            }
            else if (NewGame != null && NewGame.Quests.Contains(quest))
            {
                NewGame.Quests.Remove(quest);
            }
        }

        private async void SaveGame()
        {
            if (!CanUpdateGame())
            {

                return;
            }
            var res = await _networkManager.UpdateGame(SelectedGame);
            if (res == false)
            {
                MessageBox.Show("Ошибка Обновления");
                return;
            }

            RefreshGamesDelegate.Execute();
        }

        private async void DeleteGame()
        {
            if (_selectedGame == null)
            {
                return;
            }

            await _networkManager.DeleteGame(_selectedGame.Id);
            CreatedGames.Remove(SelectedGame);
            RefreshGamesDelegate.Execute();
        }

        private async void RefreshGames()
        {
            CreatedGames.Clear();

            var games = await _networkManager.GetGames();

            if (games == null)
            {
                return;
            }

            CreatedGames.AddRange(games);

            RaisePropertyChanged("CreatedGames");

            SelectedGame = null;
        }

        private async void CreateGame()
        {

            if (!CanCreateGame())
            {
                MessageBox.Show("Введите имя игры!");
                return;
            }

            var g = await _networkManager.CreateGame(NewGame);
            if (g != null)
            {
                MessageBox.Show("Игра добавлена!");
                NewGame = new Game { Quests = new ObservableCollection<Quest>() };
                RefreshGamesDelegate.Execute();
                return;
            }

            MessageBox.Show("Ошибка добавления");
        }

        private bool CanCreateGame()
        {
            return !String.IsNullOrWhiteSpace(NewGame?.Name);
        }

        private bool CanUpdateGame()
        {
            if (SelectedGame == null)
            {
                return false;
            }
            return !String.IsNullOrWhiteSpace(SelectedGame.Name);
        }

        private bool CanOperate()
        {
            return SelectedGame != null;
        }

        private bool CanOperate(Game g = null)
        {
            return SelectedGame != null;
        }

        private void CreateLocation(Quest quest)
        {
            Location loc = null;

            GeolocationWindow geolocationDialog = new GeolocationWindow();

            if (geolocationDialog.ShowDialog() == false)
            {
                return;
            }
            else
            {
                loc = geolocationDialog.Location;
            }

            if (loc != null)
            {
                if (quest.Location != null)
                {
                    quest.Location.Lat = loc.Lat;
                    quest.Location.Lon = loc.Lon;
                }
                else
                {
                    quest.Location = loc;
                }
            }
        }

        private void GenerateQrCodes(Game game)
        {
            if (game == null)
            {
                return;
            }

            System.Windows.Forms.FolderBrowserDialog dialog = new System.Windows.Forms.FolderBrowserDialog();
            string path = "";
            dialog.Description = "Выберите папку для сохраненния QR кодов";
            if (dialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                path = dialog.SelectedPath;
                if (string.IsNullOrWhiteSpace(dialog.SelectedPath))
                {
                    MessageBox.Show("Путь не выбран");
                    GenerateQrCodes(game);
                }

            }
            else
            {
                return;
            }

            QRCodeGenerator qrGenerator = new QRCodeGenerator();
            QRCodeData qrCodeData = null;

            StringBuilder builder = new StringBuilder();
            int i = 0;
            foreach (var quest in game.Quests)
            {
                builder.Clear();
                builder.AppendLine("Это QR приложения QR QUEST http://qrquestct.ru");
                builder.AppendLine($"qId={quest.Id}");

                qrCodeData = qrGenerator.CreateQrCode(builder.ToString(), QRCodeGenerator.ECCLevel.Q);
                QRCode qrCode = new QRCode(qrCodeData);
                Bitmap qrCodeImage = qrCode.GetGraphic(50);
                try
                {
                    qrCodeImage.Save($@"{path}\game_{game.Name.Substring(0, Math.Min(5, game.Name.Length)).Trim()}_quest_{i}.png");
                    i++;
                }
                catch (Exception e)
                {
                    MessageBox.Show(e.Message);
                    return;
                }

            }

            MessageBox.Show("Коды сгенерированны!");
        }

        public async void DialogHost_Loaded(RoutedEventArgs e)
        {
            if ((bool)await DialogHost.Show(new AuthDialog()) == false)
            {
                Application.Current.Shutdown();
                return;
            }
            else
            {
                Init();
            }
        }
    }
}
