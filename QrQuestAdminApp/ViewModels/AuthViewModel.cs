﻿using MaterialDesignThemes.Wpf;
using Prism.Commands;
using Prism.Mvvm;
using QrQuestAdminApp.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Net;
using System.Security;
using System.Windows.Input;

namespace QrQuestAdminApp.ViewModels
{
    public class AuthViewModel : BindableBase
    {
        private bool _buttonsActive = true;
        public bool ButtonsActive
        {
            get { return _buttonsActive; }
            set
            {
                _buttonsActive = value;
                RaisePropertyChanged("ButtonsActive");
            }
        }

        private string _authMessage;
        public string AuthMessage
        {
            get
            {
                return _authMessage;
            }
            set
            {
                _authMessage = value;
                RaisePropertyChanged("AuthMessage");
            }
        }
        private string _registerMessage;
        public string RegisterMessage
        {
            get { return _registerMessage; }
            set
            {
                _registerMessage = value;
                RaisePropertyChanged("RegisterMessage");
            }
        }

        private LoginViewModel _loginModel;
        public LoginViewModel LoginModel
        {
            get { return _loginModel; }
            set
            {
                _loginModel = value;
                RaisePropertyChanged("LoginModel");
            }
        }
        private RegisterViewModel _registerModel;
        public RegisterViewModel RegisterModel
        {
            get { return _registerModel; }
            set
            {
                _registerModel = value;
                RaisePropertyChanged("RegisterModel");
            }
        }

        private SecureString _loginPassword;
        public SecureString LoginPassword
        {
            get
            {
                return _loginPassword;
            }
            set
            {
                LoginModel.Password = new NetworkCredential("", value).Password;
            }
        }
        private SecureString _registerPassword;
        public SecureString RegisterPassword
        {
            get
            {
                return _registerPassword;
            }
            set
            {
                RegisterModel.Password = new NetworkCredential("", value).Password;
            }
        }
        private SecureString _registerConfirmPassword;
        public SecureString RegisterConfirmPassword
        {
            get
            {
                return _registerConfirmPassword;
            }
            set
            {
                RegisterModel.ConfirmPassword = new NetworkCredential("", value).Password;
            }
        }

        public DelegateCommand LoginDelegate { get; set; }
        public DelegateCommand RegisterDelegate { get; set; }

        NetworkManager _networkManager;

        public AuthViewModel()
        {
            _networkManager = new NetworkManager();

            RegisterModel = new RegisterViewModel();
            LoginModel = new LoginViewModel();
            LoginDelegate = new DelegateCommand(Login);
            RegisterDelegate = new DelegateCommand(Register);
        }

        private async void Login()
        {
            ButtonsActive = false;
            if (await _networkManager.GetToken(LoginModel.Login, LoginModel.Password))
            {
                DialogHost.CloseDialogCommand.Execute(true,null);
                ButtonsActive = false;
                return;
            }
            else
            {
                AuthMessage = "Ошибка входа";
            }
            ButtonsActive = true;
        }

        private async void Register()
        {
            var results = new List<System.ComponentModel.DataAnnotations.ValidationResult>();

            var context = new ValidationContext(RegisterModel, serviceProvider: null, items: null);

            RegisterMessage = string.Empty;

            if (!Validator.TryValidateObject(RegisterModel, context, results, true))
            {
                foreach (var error in results)
                {
                    RegisterMessage += error.ErrorMessage + Environment.NewLine;
                }
                return;
            }

            if (await _networkManager.Register(RegisterModel))
            {
                DialogHost.CloseDialogCommand.Execute(true, null);
                ButtonsActive = false;
                return;
            }
            else
            {
                RegisterMessage = "Ошибка регистрации";
            }

        }

    }

}
