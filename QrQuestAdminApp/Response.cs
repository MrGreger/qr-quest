﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QrQuestAdminApp
{
    public class QuestViewModel
    {
        public int Id { get; set; }
        public string Question { get; set; }
    }

    public class Point
    {
        public int Id { get; set; }
        public string Address { get; set; }
    }

    public class Response
    {
        public QuestViewModel Quest { get; set; }
        public Point Point { get; set; }
        public bool ValidAnswer { get; set; }
        public bool IsFinish { get; set; }
        public string Message { get; set; }
    }
}
