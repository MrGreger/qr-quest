﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Web.Script.Serialization;
using System.Windows;
using System.Windows.Input;
using Microsoft.Maps.MapControl.WPF;
using Location = QrQuestAdminApp.Models.Location;


namespace QrQuestAdminApp.Pages
{
    /// <summary>
    /// Логика взаимодействия для GeolocationWindow.xaml
    /// </summary>
    public partial class GeolocationWindow : Window
    {
        public Location Location { get; set; }

        private string yourip;
        private double startLat;
        private double startLon;

        public GeolocationWindow()
        {
            InitializeComponent();
            Location = new Location();
            try
            {
                using (var webClient = new System.Net.WebClient())
                {

                    var data = webClient.DownloadString("https://geoip-db.com/json");
                    JavaScriptSerializer jss = new JavaScriptSerializer();
                    var d = jss.Deserialize<dynamic>(data);

                    string ipv4 = d["IPv4"];
                    decimal latitude = d["latitude"];
                    decimal longitude = d["longitude"];

                    // Make sure you have the corresponding labels set up in your GUI
                    startLat = (double) latitude;
                    startLon = (double) longitude;
                    yourip = ipv4;
                }
            }
            catch (Exception e)
            {

            }

         

            MyMap.SetView(new Microsoft.Maps.MapControl.WPF.Location(startLat,startLon),8);

        }


        private void MyMap_OnMouseDoubleClick(object sender, MouseButtonEventArgs e)
        {

            var mousePoint = e.GetPosition(this);
            var loc = MyMap.ViewportPointToLocation(mousePoint);

            Location.Lat = loc.Latitude;
            Location.Lon = loc.Longitude;

            DialogResult = true;
        }
    }
}
