﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QrQuestModels
{
    public class LoginViewModel
    {
        public string Login { get; set; }

        public string Password { get; set; }
    }

    public class RegisterViewModel
    {
        [Required(ErrorMessage = "Введите логин", AllowEmptyStrings = false)]
        public string Username { get; set; }
        [Required(ErrorMessage = "Введите Email", AllowEmptyStrings = false)]
        [DataType(DataType.EmailAddress)]
        [EmailAddress(ErrorMessage = "Некорректный формат email адреса")]
        public string Email { get; set; }
        [Required(ErrorMessage = "Ведите пароль", AllowEmptyStrings = false)]
        [DataType(DataType.Password)]
        public string Password { get; set; }
        [Required(ErrorMessage = "Требуется подтверждение введенного пароля", AllowEmptyStrings = false)]
        [DataType(DataType.Password)]
        [Compare("Password", ErrorMessage = "Пароли не совпадают")]
        public string ConfirmPassword { get; set; }
    }

    public class GameViewModel
    {
        public GameViewModel()
        {

        }
        public GameViewModel(int id, string name, string description, string startPoint, bool bought, decimal cost)
        {
            Id = id;
            Name = name;
            Description = description;
            StartPoint = startPoint;
            Bought = bought;
            Cost = cost;
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string StartPoint { get; set; }
        public bool Bought { get; set; }
        public decimal Cost { get; set; }
    }

    public class Location
    {
        public int Id { get; set; }
        public double Lon { get; set; }
        public double Lat { get; set; }
    }

    public struct Point
    {
        public string Address { get; set; }
        public Location Location { get; set; }

        public int GameId { get; set; }
    }

    public class QuestViewModel
    {
        public int Id { get; set; }
        public string Question { get; set; }
    }

    public struct QuestAnswerValidationResponse
    {
        public bool ValidAnswer { get; set; }
        public bool IsFinish { get; set; }
        public string Message { get; set; }
    }
}
